# Project Frontend Client

This is the frontend client utilizing React to consume an API.

## Overview
This project requires the setting of two variables:
- `REACT_APP_API` - for including the API base URL
- `REACT_APP_RELEASE` - used for simple text replacement for title, e.g. value of variable: `DEV` will resullt in the page title of the HTML to have `Address Book: DEV`

## Instructions

To start a local dev environment: `npm run start`, however as noted above, for the project to work properly, environment variables must be set.

For MacOS: `REACT_APP_API=http://path/to/api REACT_APP_RELEASE=Random npm start`

For Windows (command line): `set "REACT_APP_API=http://path/to/api &  REACT_APP_RELEASE=Random" && npm start`
For more information on React & Environment Variables, see [documentation](https://facebook.github.io/create-react-app/docs/adding-custom-environment-variables#adding-temporary-environment-variables-in-your-shell)

*Note:* For optimal results, when uploading to a live environment, use the `npm build` rather than `npm start` command, and upload the contents found within the `build` folder.

## Deployment Steps
within visual studio code, set up the .env file
ran a production build

Steps for GCP:
created an account with GCP
under cloud storage, created a bucket and set up the environment
created a domain in nameCheap and configured with GCP via CNAME
created a app.yaml file
uploaded the build folder and app.yaml file within the bucket
deployed the app in the GCP shell according to [this blog post](https://medium.com/wesionary-team/deploy-your-static-react-application-on-google-cloud-platform-bc8afb9b9b6)

Steps for AWS
created an account
used an existing domain from siteground
created an S3 bucket and configured it 
uploaded build folder
pointed the domain from siteground to the aws s3 endpoint
